package com.chanchhaya.userapi.service.user;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.chanchhaya.userapi.repository.UserRepository;
import com.chanchhaya.userapi.repository.model.User;
import com.chanchhaya.userapi.utils.PageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserService implements IUserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User insert(User user) {

        logger.info("request = " + user);

        logger.info("User = " + user);

        user.setUuid(UUID.randomUUID().toString());
        user.setIsEnabled(true);

        try {
            userRepository.insert(user);
            return user;
        } catch (Exception e) {
            logger.info("User can not be created because the error has happened. Please contact the developer.");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot insert into database", e.getCause());
        }

    }

    @Override
    public User updateBiographyByUUID(User user) {
        
        User userForUpdate = userRepository.selectByUUID(user.getUuid()).orElseThrow(
            () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Record is not found", new Throwable("Record with this UUID doesn't exist in the database"))
        );

        userForUpdate.setBiography(user.getBiography());

        try {
            userRepository.updateBiographyByUUID(userForUpdate);
            return userForUpdate;
        } catch (Exception e) {
            logger.info("User can not be updated because the error has happened. Please contact the developer.");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot update into database", e.getCause());
        }

    }

    @Override
    public User deleteByUUID(String uuid) {
        
        User userForDelete = userRepository.selectByUUID(uuid).orElseThrow(
            () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Record is not found", new Throwable("Record with this UUID doesn't exist in the database"))
        );

        try {
            userRepository.deleteByUUID(uuid);
            return userForDelete;
        } catch (Exception e) {
            logger.info("User can not be deleted because the error has happened. Please contact the developer.");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot delete record from database", e.getCause());
        }

    }

    @Override
    public User selectByUUID(String uuid) {
        
        try {
            User user = userRepository.selectByUUID(uuid).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Record is not found", new Throwable("Record with this UUID doesn't exist in the database"))
            );
            return user;
        } catch (Exception e) {
            logger.info("User can not be found because the error has happened. Please contact the developer.");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot find record in database", e.getCause());
        }

    }

    @Override
    public List<User> selectAll(int size, int page) {

        List<User> users = new ArrayList<>();
        
        logger.info("size = " + size + ", page = " + page);

        if (size < 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Record is not found", new Throwable("Value of size must be positive and none-zero value"));
        }

        if (page < 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Record is not found", new Throwable("Value of page must be positive and none-zero value"));
        }

        try {
            users = userRepository.selectAll(size, page);
            logger.info("users = " + users);
            return users;
        } catch (Exception e) {
            logger.info("Users can not be found because the error has happened. Please contact the developer.");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot find records in database", e.getCause());
        }

    }

    @Override
    public PageUtils getUserPagination(PageUtils pageUtils) {
        
        int countUser = userRepository.countEnabledUser();

        pageUtils.setPagesToShow(6);
        pageUtils.setTotalPages(countUser/pageUtils.getLimit());
        pageUtils.setTotalCount(countUser);
        
        return pageUtils;

    }

}
