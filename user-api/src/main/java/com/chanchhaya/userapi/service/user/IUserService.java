package com.chanchhaya.userapi.service.user;

import java.util.List;

import com.chanchhaya.userapi.repository.model.User;
import com.chanchhaya.userapi.utils.PageUtils;

public interface IUserService {

    User insert(User user);
    
    User updateBiographyByUUID(User user);

    User deleteByUUID(String uuid);

    User selectByUUID(String uuid);
    
    List<User> selectAll(int size, int page);

    PageUtils getUserPagination(PageUtils pageUtils);

}
