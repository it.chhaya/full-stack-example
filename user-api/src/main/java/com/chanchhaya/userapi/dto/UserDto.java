package com.chanchhaya.userapi.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;
import lombok.Value;

public enum UserDTO {;
    
    private interface Id { @Positive @NotNull Integer getId(); }
    private interface Uuid { @NotBlank String getUuid(); }
    private interface Username { @NotBlank String getUsername(); }
    private interface Email { @NotBlank String getEmail(); }
    private interface Password { @NotBlank String getPassword(); }
    private interface ConfirmedPassword { @NotBlank String getConfirmedPassword(); }
    private interface Biography { String getBiography(); }
    private interface IsEnabled { Boolean getIsEnabled(); }

    public enum Request {;

        @Data
        public static class UpdateBiography implements Biography {
            String biography;
        }

        @Value
        public static class Create implements Username, Email, Password, ConfirmedPassword, Biography {
            String username;
            String email;
            String password;
            String confirmedPassword;
            String biography;
        }

    }

    public enum Response {;

        @Value public static class Public implements Uuid, Email, Biography {
            String uuid;
            String email;
            String biography;
        }

        @Value public static class Private implements Id, Uuid, Username, Email, Password, Biography, IsEnabled {
            Integer id;
            String uuid;
            String username;
            String email;
            String password;
            String biography;
            Boolean isEnabled;
        }

        @Value public static class DeleteOperation implements Username, Email {
            String username;
            String email;
        }

        @Value public static class PublicAsList implements Uuid, Email, Biography, IsEnabled {
            String uuid;
            String email;
            String biography;
            Boolean isEnabled;
        }

    }

}
