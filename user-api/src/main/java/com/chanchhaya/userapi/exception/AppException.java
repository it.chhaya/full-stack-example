package com.chanchhaya.userapi.exception;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chanchhaya.userapi.api.response.ErrorResponseModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class AppException {

    private Logger logger = LoggerFactory.getLogger(AppException.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponseModel> handleJsonMappingException(HttpMessageNotReadableException e) {

        logger.error("Error has been happened while trying to map JSON from client");

        ErrorResponseModel response = new ErrorResponseModel();
        List<Map<String,String>> errors = new ArrayList<>();
        Map<String,String> fieldError = new HashMap<>();

        fieldError.put("message", e.getMessage());

        errors.add(fieldError);

        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setMessage("Error has been happened while trying to map JSON from client");
        response.setErrors(errors);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseModel> handleDataInvalidException(MethodArgumentNotValidException e) {

        ErrorResponseModel response = new ErrorResponseModel();
        List<Map<String,String>> errors = new ArrayList<>();
        Map<String,String> messageOfFields = new HashMap<>();

        e.getBindingResult().getAllErrors().forEach((error) -> {
            messageOfFields.put(((FieldError)error).getField(), error.getDefaultMessage());
        });

        errors.add(messageOfFields);

        response.setMessage("Request data is invalid, please check in error details");
        response.setCode(HttpStatus.BAD_REQUEST.value());
        response.setErrors(errors);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ErrorResponseModel> handleServiceException(ResponseStatusException e) {

        ErrorResponseModel response = new ErrorResponseModel();
        List<Map<String,String>> errors = new ArrayList<>();
        Map<String,String> message = new HashMap<>();

        message.put("message", e.getCause().getLocalizedMessage());
        errors.add(message);

        response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.setMessage(e.getReason());
        response.setErrors(errors);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

    
}
