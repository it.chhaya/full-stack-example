package com.chanchhaya.userapi.repository;

import java.util.List;
import java.util.Optional;

import com.chanchhaya.userapi.repository.model.User;
import com.chanchhaya.userapi.repository.provider.UserProvider;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {
    
    @InsertProvider(type = UserProvider.class, method = "buildInsert")
    void insert(@Param("user") User user);

    @UpdateProvider(type = UserProvider.class, method = "buildUpdateBiographyByUUID")
    void updateBiographyByUUID(@Param("user") User user);

    @DeleteProvider(type = UserProvider.class, method = "buildDeleteByUUID")
    void deleteByUUID(@Param("uuid") String uuid);

    @SelectProvider(type = UserProvider.class, method = "buildSelectByUUID")
    Optional<User> selectByUUID(@Param("uuid") String uuid);

    @SelectProvider(type = UserProvider.class, method = "buildSelectAll")
    List<User> selectAll(@Param("size") int size, @Param("page") int page);

    @Select("SELECT COUNT(*) FROM users WHERE is_enabled = TRUE")
    int countEnabledUser();

}
