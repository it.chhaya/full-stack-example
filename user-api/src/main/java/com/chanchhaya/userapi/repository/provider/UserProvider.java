package com.chanchhaya.userapi.repository.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    
    public String buildInsert() {

        return new SQL() {{
            INSERT_INTO("users");
            VALUES("uuid", "#{user.uuid}");
            VALUES("username", "#{user.username}");
            VALUES("email", "#{user.email}");
            VALUES("password", "#{user.password}");
            VALUES("biography", "#{user.biography}");
        }}.toString();

    }

    public String buildUpdateBiographyByUUID() {
        return new SQL() {{
            UPDATE("users");
            SET("biography = #{user.biography}");
            WHERE("uuid = #{user.uuid}");
        }}.toString();
    }

    public String buildDeleteByUUID() {
        return new SQL() {{
            DELETE_FROM("users");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildSelectByUUID() {
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String buildSelectAll(@Param("page") int page) {
        int pageForPostgres = page - 1;
        return new SQL() {{
            SELECT("*");
            FROM("users");
            WHERE("is_enabled = TRUE");
            OFFSET(pageForPostgres);
            LIMIT("#{size}");
        }}.toString();
    }

}
