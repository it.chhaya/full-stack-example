package com.chanchhaya.userapi.api.response;

import java.sql.Timestamp;
import java.util.List;

public class BaseResponseModel<T> {
    
    private Integer code;
    private List<String> messages;
    private Timestamp timestamp;
    private T data;

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<String> getMessages() {
        return this.messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
            " code='" + getCode() + "'" +
            ", messages='" + getMessages() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", data='" + getData() + "'" +
            "}";
    }

}
