package com.chanchhaya.userapi.api.response;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class ErrorResponseModel {
    
    private Integer code;
    private String message;
    private Timestamp timestamp;
    private List<Map<String, String>> errors;

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public List<Map<String,String>> getErrors() {
        return this.errors;
    }

    public void setErrors(List<Map<String,String>> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "{" +
            " code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", errors='" + getErrors() + "'" +
            "}";
    }

}
