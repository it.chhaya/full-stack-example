package com.chanchhaya.userapi.api.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import com.chanchhaya.userapi.api.response.ApiResponse;
import com.chanchhaya.userapi.api.response.BaseResponseModel;
import com.chanchhaya.userapi.dto.UserDTO;
import com.chanchhaya.userapi.dto.UserDTO.Response.Public;
import com.chanchhaya.userapi.repository.model.User;
import com.chanchhaya.userapi.service.user.UserService;
import com.chanchhaya.userapi.utils.PageUtils;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    private Logger logger = LoggerFactory.getLogger(UserRestController.class);
    private ModelMapper modelMapper;

    private UserService userService;

    @Autowired
    public UserRestController(ModelMapper modelMapper, UserService userService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<BaseResponseModel<UserDTO.Response.Public>> insert(
            @Valid @RequestBody UserDTO.Request.Create request) {

        BaseResponseModel<UserDTO.Response.Public> response = new BaseResponseModel<>();

        if (!request.getPassword().equals(request.getConfirmedPassword())) {
            logger.info("User can not be created because the password is not matched");
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "User can not be created, please check the errros", new Throwable("Password is not matched"));
        }

        logger.info("Body = " + request);

        User user = modelMapper.map(request, User.class);

        logger.info("User = " + user);

        user = userService.insert(user);

        logger.info("User after inserted = " + user);

        UserDTO.Response.Public publicRepsonse = new UserDTO.Response.Public(user.getUuid(), user.getEmail(),
                user.getBiography());

        logger.info("publicResponse which created from user = " + publicRepsonse);

        response.setCode(HttpStatus.OK.value());
        response.setMessages(Arrays.asList("Record is inserted successfully!"));
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setData(publicRepsonse);

        return ResponseEntity.ok(response);

    }

    @PutMapping("/{uuid}")
    public ResponseEntity<BaseResponseModel<UserDTO.Response.Public>> updateBiographyByUUID(
            @Valid @RequestBody UserDTO.Request.UpdateBiography body, @PathVariable("uuid") String uuid) {

        BaseResponseModel<UserDTO.Response.Public> response = new BaseResponseModel<>();

        logger.info("body = " + body);

        User user = modelMapper.map(body, User.class);
        user.setUuid(uuid);

        logger.info("user = " + user);

        // call service to update
        user = userService.updateBiographyByUUID(user);

        // retreive data back and setup data to response
        UserDTO.Response.Public publicResponse = new UserDTO.Response.Public(user.getUuid(), user.getEmail(),
                user.getBiography());

        logger.info("publicResponse = " + publicResponse);

        response.setCode(HttpStatus.OK.value());
        response.setMessages(Arrays.asList("Record is updated successfully!"));
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setData(publicResponse);

        return ResponseEntity.ok(response);

    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<BaseResponseModel<UserDTO.Response.DeleteOperation>> deleteByUUID(
            @PathVariable("uuid") String uuid) {

        BaseResponseModel<UserDTO.Response.DeleteOperation> response = new BaseResponseModel<>();

        logger.info("uuid = " + uuid);

        User user = userService.deleteByUUID(uuid);

        logger.info("user = " + user);

        UserDTO.Response.DeleteOperation deleteOperation = new UserDTO.Response.DeleteOperation(user.getUsername(),
                user.getEmail());

        logger.info("deleteOperation = " + deleteOperation);

        response.setCode(HttpStatus.OK.value());
        response.setMessages(Arrays.asList("Record is deleted successfully"));
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setData(deleteOperation);

        return ResponseEntity.ok(response);

    }

    @GetMapping("/{uuid}")
    public ResponseEntity<BaseResponseModel<UserDTO.Response.Public>> selectByUUID(@PathVariable("uuid") String uuid) {

        BaseResponseModel<UserDTO.Response.Public> response = new BaseResponseModel<>();

        logger.info("uuid = " + uuid);

        User user = userService.selectByUUID(uuid);

        logger.info("user = " + user);

        UserDTO.Response.Public data = new UserDTO.Response.Public(user.getUuid(), user.getEmail(),
                user.getBiography());

        response.setCode(HttpStatus.OK.value());
        response.setMessages(Arrays.asList("Record is found successfully"));
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setData(data);

        return ResponseEntity.ok(response);

    }

    @GetMapping
    public ResponseEntity<ApiResponse.ResponseList<List<UserDTO.Response.Public>>> selectAll(@RequestParam("size") int size,
            @RequestParam("page") int page) {

        ApiResponse.ResponseList<List<UserDTO.Response.Public>> response = null;
        List<UserDTO.Response.Public> data = new ArrayList<>();

        logger.info("size = " + size + ", page = " + page);

        List<User> users = userService.selectAll(size, page);

        logger.info("users = " + users);

        users.forEach((user) -> {
            data.add(new UserDTO.Response.Public(user.getUuid(), user.getEmail(), user.getBiography()));
        });

        logger.info("data = " + data);

        PageUtils pageUtils = userService.getUserPagination(new PageUtils(size, page));

        response = new ApiResponse.ResponseList<List<Public>>(HttpStatus.OK.value(),
                    "Record is found successfully",
                    new Timestamp(System.currentTimeMillis()),
                    data,
                    pageUtils);

        logger.info("response = " + response);

        return ResponseEntity.ok(response);

    }

}
