package com.chanchhaya.userapi.api.response;

import java.sql.Timestamp;

import com.chanchhaya.userapi.utils.PageUtils;

import lombok.Value;

public enum ApiResponse {;

    private interface Code { Integer getCode(); }
    private interface Message { String getMessage(); }
    private interface RequestTime { Timestamp getRequestTime(); }
    private interface Data<T> { T getData(); }
    private interface Pagination { PageUtils getPageUtils(); }

    @Value public static class ResponseList<T> implements Code, Message, RequestTime, Data<T>, Pagination {
        Integer code;
        String message;
        Timestamp requestTime;
        T data;
        PageUtils pageUtils;
    }
    
}
